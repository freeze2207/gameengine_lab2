#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H
#include <iostream>

class AssetManager
{
public:
	AssetManager();
	~AssetManager();

	void initialize();
	void update();
	void display();
};

#endif // !ASSETMANAGER_H
