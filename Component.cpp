#include "Component.h"

Component::Component()
	:id(0)
{
	
}

Component::~Component()
{
}

void Component::initialize(int id)
{
	this->id = id;
}

int Component::getComponentId()
{
	return 0;
}

void Component::update()
{
}

void Component::display()
{
	std::cout << "\t\t\t\tid: " << this->id << std::endl;
}
