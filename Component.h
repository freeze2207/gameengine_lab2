#ifndef COMPONENT_H
#define COMPONENT_H

#include <iostream>

class Component
{
private:
	int id = 0;

public:
	Component();
	~Component();

	void initialize(int id);

	int getComponentId();

	void update();

	void display();
};

#endif // !CPMPONENT_H
