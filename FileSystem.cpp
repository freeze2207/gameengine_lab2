#include "FileSystem.h"
#include <iostream>
#include <fstream>
#include "json.hpp"

FileSystem::FileSystem()
	:documentPathToLoad(new std::string())
{
}

FileSystem::~FileSystem()
{
	delete documentPathToLoad;
}

void FileSystem::initialize()
{

}

void FileSystem::update()
{
}

void FileSystem::display()
{
	std::cout << "File System" << std::endl;
}

// File System creates Render System, I think let Game Engine create instead better?
// I had some fatal linker errors including json.hpp in header files, so I decide to pass a RenderSystem pointer instead
// TODO fix json header inclusion issue
RenderSystem* FileSystem::LoadGameSetting()
{
	std::ifstream inputStream("GameSettings.json");
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON document;
	RenderSystem* rs = nullptr;
	
	if (!str.empty())
	{
		document = json::JSON::Load(str);
	}else
	{
		std::cout << "Error in loading Game Setting file" << std::endl;
		return nullptr;
	}

	if (document.hasKey("GameEngine") && !document["GameEngine"]["DefaultFile"].ToString().empty())
	{
		*documentPathToLoad = document["GameEngine"]["DefaultFile"].ToString();

	}
	
	if (document.hasKey("RenderSystem") && !document["RenderSystem"]["Name"].ToString().empty() 
		&& document["RenderSystem"]["width"].ToInt() && document["RenderSystem"]["height"].ToInt()
		&& document["RenderSystem"].hasKey("fullscreen"))
	{
		rs = new RenderSystem(document["RenderSystem"]["Name"].ToString(),
							  document["RenderSystem"]["width"].ToInt(),
							  document["RenderSystem"]["height"].ToInt(),
							  document["RenderSystem"]["fullscreen"].ToBool());
	}
	
	inputStream.close();

	return rs;
}

std::list<GameObject*>* FileSystem::LoadFile(std::string fileName)
{
	std::ifstream inputStream(fileName);
	std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
	json::JSON document;
	std::list<GameObject*>* gameObjectList = nullptr;
	

	if (!str.empty())
	{
		document = json::JSON::Load(str);
	}
	else
	{
		std::cout << "Error in loading " << fileName << std::endl;
		return gameObjectList;
	}

	if (document.hasKey("GameObjects"))
	{
		gameObjectList = new std::list<GameObject*>();

		for (auto& gameObject : document["GameObjects"].ArrayRange())	// Loop for GameObjects
		{
			if (gameObject.hasKey("className") && !gameObject["Name"].ToString().empty()
				&& gameObject.hasKey("Components"))
			{
				std::list<Component*> componentsList;
				for (auto& component : gameObject["Components"].ArrayRange())	// Loop for components
				{

					Component* tempComponent = new Component();
					tempComponent->initialize(component["id"].ToInt());
					componentsList.push_back(tempComponent);
				}

				GameObject* tempGameObject = new GameObject();
				tempGameObject->initialize(gameObject["Name"].ToString(), componentsList);
				(*gameObjectList).push_back(tempGameObject);
			}
		}
		
	}


	inputStream.close();
	return gameObjectList;
}