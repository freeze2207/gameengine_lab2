#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <string>
#include <list>
#include "RenderSystem.h"
#include "GameObject.h"
#include "Component.h"

class FileSystem
{
private:

public:
	std::string* documentPathToLoad = nullptr;

	FileSystem();
	~FileSystem();

	void initialize();
	void update();
	void display();
	RenderSystem* LoadGameSetting();
	std::list<GameObject*>* LoadFile(std::string fileName);

};

#endif // !FILESYSTEM_H
