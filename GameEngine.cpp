#include "GameEngine.h"
#include <iostream>
#include <ctime>

GameEngine::GameEngine()
	:renderSystem(nullptr), fileSystem(nullptr), inputManager(nullptr), assetManager(nullptr), gameObjectManager(nullptr)
{

	//std::cout << "Game Engine constructed." << std::endl;
}

GameEngine::~GameEngine()
{
	delete renderSystem;
	delete fileSystem;
	delete inputManager;
	delete assetManager;
	delete gameObjectManager;

	//std::cout << "Game Engine destroyed." << std::endl;
}

void GameEngine::initialize()
{
	fileSystem = new FileSystem();
	// init for future
	fileSystem->initialize();
	

	renderSystem = fileSystem->LoadGameSetting();
	if (renderSystem != nullptr)
	{
		// init for future
		renderSystem->initialize();
	}
	else
	{
		std::cout << "NULL PTR for renderSystem" << std::endl;
	}
	
	inputManager = new InputManager();
	// init for future
	inputManager->initialize();

	assetManager = new AssetManager();
	// init for future
	assetManager->initialize();


	std::list<GameObject*>* gameObjectManagerPointer = nullptr;
	if ( !(fileSystem->documentPathToLoad->empty()) )
	{
		gameObjectManagerPointer = fileSystem->LoadFile(*(fileSystem->documentPathToLoad));
	}
	
	gameObjectManager = new GameObjectManager();
	// init for future

	if (gameObjectManagerPointer != nullptr)
	{
		gameObjectManager->initialize(*gameObjectManagerPointer);
	}
	else
	{
		std::cout << "NULL PTR for gameObjectManager" << std::endl;
	}
	
	std::cout << "Game Engine initialized." << std::endl;
}

void GameEngine::GameLoop()
{
	std::clock_t startTime;
	startTime = std::clock();
	std::cout << "GameLoop is running..." << std::endl;

	// running for 10s
	while (true)
	{
		renderSystem->update();
		fileSystem->update();
		assetManager->update();
		inputManager->update();
		gameObjectManager->update();
		if ((std::clock() - startTime) / (int)CLOCKS_PER_SEC > 10)
		{
			break;
		}
	}
}

void GameEngine::display()
{
	std::cout << "Game Engine" << std::endl;
	renderSystem->display();
	fileSystem->display();
	assetManager->display();
	inputManager->display();
	gameObjectManager->display();
}
