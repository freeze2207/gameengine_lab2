#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H

#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"

class GameEngine
{
private:
	RenderSystem* renderSystem = nullptr;
	FileSystem* fileSystem = nullptr;
	InputManager* inputManager = nullptr;
	AssetManager* assetManager = nullptr;
	GameObjectManager* gameObjectManager = nullptr;

public:
	GameEngine();
	~GameEngine();

	void initialize();
	void GameLoop();
	void display();
};

#endif // !GAME_ENGINE_H