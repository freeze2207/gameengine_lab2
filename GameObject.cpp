#include "GameObject.h"

GameObject::GameObject()
    :name("")
{
}

GameObject::~GameObject()
{
    for (std::list<Component*>::iterator i = components.begin(); i != components.end(); i++)
    {
        delete* i;
    }
    components.clear();
}

void GameObject::initialize(std::string name, std::list<Component*> components)
{
    this->name = name;
    this->components = components;
}

void GameObject::addComponent(Component* component)
{
}

void GameObject::removeComponent(Component* component)
{
}

void GameObject::update()
{
    for (Component* component : components)
    {
        component->update();
    }
}

void GameObject::display()
{   
    std::cout << "\t\tGameObject " << std::endl;
    std::cout << "\t\t\tname: " << this->name << std::endl;
    std::cout << "\t\t\tComponents: " << std::endl;
    for (Component* component : components)
    {
        component->display();
    }
}

std::string& GameObject::getName()
{
    
    // TODO: insert return statement here
    return this->name;
}
