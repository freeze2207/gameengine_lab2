#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "Component.h"
#include <iostream>
#include <list>
#include <string>

class GameObject
{
private:
	std::list<Component*> components;
	std::string name = NULL;

public:
	GameObject();
	~GameObject();

	void initialize(std::string name, std::list<Component*> components);
	
	void addComponent(Component* component);
	void removeComponent(Component* component);

	void update();

	void display();

	std::string& getName();
};

#endif // !GAMEOBJECT_H
