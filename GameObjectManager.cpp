#include "GameObjectManager.h"

GameObjectManager::GameObjectManager()
{
}

GameObjectManager::~GameObjectManager()
{
	// free GameObjects
	for ( std::list<GameObject*>::iterator i = gameObjects.begin(); i != gameObjects.end(); i++)
	{
		delete *i;
	}
	gameObjects.clear();

}

void GameObjectManager::initialize(std::list<GameObject*> gameObjects)
{
	this->gameObjects = gameObjects;
}

void GameObjectManager::update()
{
	for (GameObject* gameObject : gameObjects)
	{
		gameObject->update();
	}
}

void GameObjectManager::addGameObject(GameObject* gameObject)
{
}

void GameObjectManager::removeGameObject(GameObject* gameObject)
{
}

void GameObjectManager::display()
{
	std::cout << "Game Object Manager" << std::endl;
	std::cout << "\tGameObjects:" << std::endl;
	for (GameObject* gameObject : gameObjects)
	{
		gameObject->display();
	}
}
