#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include "GameObject.h"
#include <iostream>
#include <list>


class GameObjectManager
{
private:
	std::list<GameObject*> gameObjects;

public:
	GameObjectManager();
	~GameObjectManager();

	void initialize(std::list<GameObject*> gameObjects);
	void update();

	void addGameObject(GameObject* gameObject);
	void removeGameObject(GameObject* gameObject);

	void display();
};

#endif // !GAMEOBJECTMANAGER_H
