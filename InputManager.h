#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <iostream>

class InputManager
{
public:
	InputManager();
	~InputManager();

	void initialize();
	void update();
	void display();
};

#endif // !INPUTMANAGER_H
