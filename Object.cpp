#include "Object.h"

Object::Object()
    :name(""), initialized(false)
{
}

Object::~Object()
{
}


bool Object::isInitialized()
{
    return initialized;
}

std::string& Object::getName()
{
    // TODO: insert return statement here
    return this->name;
}

void Object::initialize()
{
    initialized = true;
}

void Object::display()
{
}
