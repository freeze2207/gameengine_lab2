#ifndef OBJECT_H
#define OBJECT_H

#include <string>

class Object
{
private:
	bool initialized = false;
	std::string name = "";

public:
	Object();
	~Object();
	
	bool isInitialized();
	std::string& getName();
	void initialize();
	void display();
};

#endif // !OBJECT_H
