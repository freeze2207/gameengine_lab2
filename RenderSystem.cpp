#include "RenderSystem.h"

RenderSystem::RenderSystem(std::string name, int width, int height, bool isFullScreen)
	: name(name), width(width), height(height), fullscreen(isFullScreen)
{

}

RenderSystem::~RenderSystem()
{
}

void RenderSystem::initialize()
{
}

void RenderSystem::update()
{
}

void RenderSystem::display()
{
	std::cout << "Render System" << std::endl;
	std::cout << "\t Name: " << this->name << std::endl;
	std::cout << "\t Width: " << this->width << std::endl;
	std::cout << "\t Height: " << this->height << std::endl;
	std::cout << "\t FullScreen: "<< (this->fullscreen == true ? "True" : "False") << std::endl;
}
