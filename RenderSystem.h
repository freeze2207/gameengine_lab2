#ifndef RENDERSYSTEM_H
#define RENDERSYSTEM_H

#include <iostream>
#include <string>

class RenderSystem
{
private:
	std::string name = "";
	int width = 0;
	int height = 0;
	bool fullscreen = false;

public:
	RenderSystem(std::string name, int width, int height, bool isFullScreen);
	~RenderSystem();

	void initialize();
	void update();
	void display();
};

#endif // !RENDERSYSTEM_H
